#ifndef AIR_DATA_TYPES
#define AIR_DATA_TYPES

#include <Arduino.h>

// used for API communication
typedef struct {
  float temperature; // -50.0 - +50.0
  float humidity;    // 0.0 - 100.0
  uint16_t co2;      // 300 - 3000
} AirQualityRecord;

typedef struct {
  AirQualityRecord *air;
  uint32_t unixtime;
} TimedAirQualityRecord;

#endif //AIR_DATA_TYPES
