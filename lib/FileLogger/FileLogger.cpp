#include "FileLogger.h"
#include "FileLoggerDateTimeCallback.h"

// this is needed for querying the reset reason
#include "user_interface.h"

/**
 * Compares two date strings of the pattern "YYYY-MM-DD"
 * @param a,b pointer to 0 terminated string
 * @returns 0  if equal
 *          <0 if a before b
 *          >0 if a after  b
 */
int DateCompare(const void* a, const void* b)
{
    // DateTime dateA(*a);
    // DateTime dateb(*b);
  char const *date_a = *(char const **)a;
  char const *date_b = *(char const **)b;
  return strcmp(date_a, date_b);

}

/**
 * Compares timestamps of the pattern "hh:mm:ss"
 * @param a,b pointer to 0 terminated string
 * @returns
 * a earlier as  b => < 0
 * a later as    a => > 0
 * a == b          => 0
 */
int TimeCompare(const char* a, const char* b) {
  if(strnlen(a, 8) != 8) return 99;
  if(strnlen(b, 8) != 8) return 199;
  char timePartA[] = "aa";
  char timePartB[] = "bb";
  for (uint8_t partPtr=0; partPtr<7; partPtr += 3){
    memcpy(timePartA, a+partPtr, 2);
    memcpy(timePartB, b+partPtr, 2);

    int result = strcmp(timePartA, timePartB);
    if (result) {
      return result;
    }
  }
  return 0;
}

/**
 * A logger which uses the user memory for storing a limited
 * number of records intermediatly until the records
 * are persited to log files on a SD card
 *
 */
FileLogger::FileLogger(uint8_t rtcMemOffsetStorePosition)
    : rtcMemOffsetStorePostion(rtcMemOffsetStorePosition) {

// the first bucket's lower 16bit are used for pointing to the
// next avaiable user memory position within the rtc for storing
// a data record
// the higher 16 bits store the minute value of the last
// logged record
  if(ESP.getResetInfoPtr()->reason != REASON_DEEP_SLEEP_AWAKE) {
    // indicate that no record has been logged so far with
    // 60 since valid minutes are 0-59
    setLastLoggedMinute(60);
    setRtcMemDataRecordOffset(0);
  }
}

FileLogger::~FileLogger() {
}

bool FileLogger::begin() {
  // Initialize SD.
  if (!sd.begin(SD_CONFIG)) {
    Serial.print("WARN FL: SD could not successfully be initialized\n");
    return false;
    // sd.initErrorHalt(&Serial);
  }
  FsDateTime::setCallback(dateTimeCallback);
  return true;
}

/**
 * Add a record to the user memory for intermediate storage
 */
void FileLogger::addDataPoint(DateTime *dateTime, AirQualityRecord *record) {

  // store hour:minute:second-temp,humid,co2 to next free memory slot
  RtcMemAirLogRecord airMemRecord;
  airMemRecord.temperature = (record->temperature * 10U);
  airMemRecord.humidity = (record->humidity * 10U);
  airMemRecord.co2 = record->co2;
  airMemRecord.hour = dateTime->hour();
  airMemRecord.minute = dateTime->minute();
  airMemRecord.second = dateTime->second();

  uint16_t rtcMemDataRecordOffset = getRtcMemDataRecordOffset();
  if (writeRecordToRtcUserMemory(rtcMemDataRecordOffset, &airMemRecord)) {
    // adjusting record pointer to point to next free bucket in RTC user mem.
    setLastLoggedMinute(airMemRecord.minute);
    rtcMemDataRecordOffset++;
    setRtcMemDataRecordOffset(rtcMemDataRecordOffset);
  }
}

/**
 * Persist all intermediately stored record to a logfile
 * on the sd card.
 * The name of the logfile is generated from the date passed in.
 */
bool FileLogger::writeValuesToFile(DateTime *dateTime) {

  uint16_t bucketNumber = getRtcMemDataRecordOffset();
  if (bucketNumber == 0) {
    Serial.print("FL::writeValuesToFile: Bucket: 0");
    return true;
  }

  if(!waitForSDCardReady()){
    return false;
  }

  uint8_t currentHour = dateTime->hour();
  file_t logFileToday;
  file_t logFileYesterday;

  char date[] = "YYYY-MM-DD_air.log";// DateTime::TIMESTAMP_DATE;
  dateTime->toString(date);
  if (!logFileToday.open(date, O_RDWR | O_CREAT | O_APPEND)) {
    Serial.printf("Failed to open file %s \n", date);
    return false;
  }
  // open yesterdays log file only within the first half an hour
  // of a day
  if (currentHour == 0 && dateTime->minute() < 30) {
    char nameYesterday[] = "YYYY-MM-DD_air.log";
    DateTime yesterday = *dateTime - TimeSpan(1, 0, 0, 0);
    yesterday.toString(nameYesterday);
    if (!logFileYesterday.open(nameYesterday, O_RDWR | O_CREAT | O_APPEND)) {
      Serial.printf("Failed to open file %s \n", nameYesterday);
      return false;
    }
  }

  RtcMemAirLogRecord airMemLogRecord;
  for (uint8_t r=0; r < bucketNumber; r++ ) {
    readRecordFromRtcUserMemory(r, &airMemLogRecord);

    // e.g method is called at 0:00 and rtc user mem contains
    // data from 23:52, 23:54, 23:56, 23:58 and 0:00
    if (airMemLogRecord.hour > currentHour && logFileYesterday.isOpen()) {
      writeRecordToLogFile(&logFileYesterday, &airMemLogRecord);
    } else {
      writeRecordToLogFile(&logFileToday, &airMemLogRecord);
    }
  }

  logFileToday.close();
  logFileYesterday.close();

#ifdef DEBUG_PERSIST
  RtcMemAirLogRecord airMemRecord;
  for (uint8_t r=0; r < getRtcMemDataRecordOffset(); r++ ) {
    readRecordFromRtcUserMemory(r, &airMemRecord);
    printLogRecord(&airMemRecord);
  }
#endif
  setRtcMemDataRecordOffset(0);
  return true;
}

void FileLogger::writeRecordToLogFile(file_t *logFile, RtcMemAirLogRecord *record) {
  logFile->printf("%02u:%02u:%02u, ", record->hour, record->minute, record->second);
  logFile->print(record->temperature / 10.0F, 1);
  logFile->print(", ");
  logFile->print(record->humidity / 10.0F, 1);
  logFile->print(", ");
  logFile->printf("%4u\n", record->co2);
}

/**
 * Retriev all records which not have been retrieved before, calling the
 * callback function for every record.
 * @param parameter Will be passed to the callback function (could be e.g. the caller)
 */
bool FileLogger::retrieveData(recordCallbackFunction callbackFunction, void* parameter) {

  if(callbackFunction == nullptr) return false;

  if (!waitForSDCardReady()) {
    Serial.print("WARN FL::retrieveData: SD not ready\n");
    return false;
  }

  char* dateOfLastRecordSent = nullptr;
  char dateOfLastRecordSentFormat[] = "YYYY-MM-DD";
  char timeOfLastRecordSent[] = "hh:mm:ss";
  if(sd.exists(LAST_RECORD_SENT_FILE)) {
    file_t lastSendRecordFile;
    if (!lastSendRecordFile.open(LAST_RECORD_SENT_FILE, O_READ)) {
      Serial.printf("FL::retrieveData: Failed to open existing 'lastSendRecord.txt'\n");
      return false;
    }
    char EOL[2] = "\n";
    char previousSendRecordTimeBuffer[20];
    int readCount = lastSendRecordFile.fgets(previousSendRecordTimeBuffer, 20, EOL);
    lastSendRecordFile.close();

    if (readCount > 18) {
#ifdef DEBUG
      Serial.printf("FL::retrieveData: timestamp of last record sent : %s - length(%i)\n",
              previousSendRecordTimeBuffer, readCount);
#endif
      DateTime dateTimeOfLastSentRecord(previousSendRecordTimeBuffer);

      if (dateTimeOfLastSentRecord.isValid()) {
        dateOfLastRecordSent = dateOfLastRecordSentFormat;
        dateTimeOfLastSentRecord.toString(dateOfLastRecordSent);
        dateTimeOfLastSentRecord.toString(timeOfLastRecordSent);
      }
    }
  }
#ifdef DEBUG
  if(dateOfLastRecordSent == nullptr) {
    Serial.print("FL::retrieveData: timestamp of previous sending not available\n");
  } else {
    Serial.printf("FL: Date of last record sent: %s\n", dateOfLastRecordSent);
    Serial.printf("FL: Time of last record sent: %s\n", timeOfLastRecordSent);
  }
#endif

  // scrape root of SD card for unprocessed or partly processed logFiles and
  // store date part of each logfile found in this buffer.
  char *logFileBuffer[MAX_LOGFILES];
  uint8_t numberLogFiles = parseRootForUnprocessedLogFiles(logFileBuffer, MAX_LOGFILES, dateOfLastRecordSent);

#ifdef DEBUG
  Serial.printf("FL: Found %u applicable log files for processing.\n", numberLogFiles);
#endif

  if (numberLogFiles > 0) {
    if(numberLogFiles > 1) {
      // sort files by date if more than one applicable file found
      // otherwise the storage of the last record sent could get
      // out of order, messing things up next time the upload ist called again.
      qsort( logFileBuffer, numberLogFiles, sizeof(char*), DateCompare);
    }


    char dateTimeOfLastRecordSent[20]; // storage for the date and time of the last record sent
    // processing logfiles one after another
    for (uint8_t f=0; f < numberLogFiles; f++) {
      uint16_t recordsUploaded = 0;

      char timeOfLastProcessedRecord[9];
      if ((dateOfLastRecordSent != nullptr) && strncmp(logFileBuffer[f], dateOfLastRecordSent, 10) == 0) {
        // if this file as been processed previously (the last time an upload was triggered)
        // the time of the last processed record needs to be passed in as an offset
        // to prevent sending records twice
        recordsUploaded = processLogFile(logFileBuffer[f], timeOfLastProcessedRecord, timeOfLastRecordSent,
                              callbackFunction, parameter);
      } else {
        // no offset, send everything.
        recordsUploaded = processLogFile(logFileBuffer[f], timeOfLastProcessedRecord, nullptr,
                              callbackFunction, parameter);
      }

      if(recordsUploaded > 0) {
        memcpy(dateTimeOfLastRecordSent, logFileBuffer[f], 10);
        memcpy(dateTimeOfLastRecordSent+11, timeOfLastProcessedRecord, 8);
        dateTimeOfLastRecordSent[10] = 'T';
        dateTimeOfLastRecordSent[19] = 0;
      }

      free(logFileBuffer[f]);
    }

    if (DateTime(dateTimeOfLastRecordSent).isValid()) {
      file_t lastSendRecordFile;
      lastSendRecordFile.open(LAST_RECORD_SENT_FILE, O_CREAT | O_WRITE);
      lastSendRecordFile.print(dateTimeOfLastRecordSent);
      lastSendRecordFile.close();
    } // if not something went wrong but no recovering is possible
  }

  return true;

}

/**
 * Process a single logfile
 * @param date the date of the logfile to be processed
 * @param timestampOfLastRecordSent pointer to buffer of size (9 chars) where
 *                                  the time of the last processed record is copied into
 * @param timestampOffset if not nullptr, only records with a later time than the offset
 *                        will be considered for processing
 * @param callbackFunctin the function called for every record
 * @param callbackParameter user defined parameter passed into the callback function as
 *                          second parameter
 * @return number of records processed
 */
uint16_t FileLogger::processLogFile(const char* date, char* timestampOfLastRecordSent, const char* timestampOffset,
                    recordCallbackFunction callbackFunction, void* callbackParameter) {
  file_t logFile;
  char fileName[] = "YYYY-MM-DD_air.log";
  memcpy(fileName, date, 10);
  if(!logFile.open(fileName, O_READ)) {
#ifdef DEBUG
    Serial.printf("WARN FL: failed to open logfile %s\n", fileName);
#endif
    return 0;
  }
#ifdef DEBUG
  Serial.printf("FL: ====> Processing logfile %s <====\n", fileName);
#endif
  char lineBuffer[MAX_LOGENTRY_LENGHT];
  char EOL[2] = "\n";

  // every record needs a complete timestamp inclusive the date
  // for further processing (e.g. persisting to the db)
  // since it's only implicitly stored within the filename
  // and it's equal for every record included in the file
  // the date part is copied into it here
  char timeStampCurrentLogEntry[] = "YYYY-MM-DDThh:mm:ss";
  memcpy(timeStampCurrentLogEntry, date, 10);


  uint16_t recordsUploaded = 0;
  while(logFile.available32()) {
    logFile.fgets(lineBuffer, MAX_LOGENTRY_LENGHT, EOL);
#ifdef DEBUG_RECORD
    Serial.printf("FL: #> %s", lineBuffer);
#endif
    AirQualityRecord airQualityRecord;
    if (parseLogFileLine(lineBuffer, &airQualityRecord)) {
#ifdef DEBUG
      Serial.printf("FL: parsed record ->%.1f, %.1f, %d<-\n"
        , airQualityRecord.temperature, airQualityRecord.humidity, airQualityRecord.co2);
#endif
      TimedAirQualityRecord timedRecord;
      // complete the recod's time stamp with the
      // record's time
      memcpy(timeStampCurrentLogEntry+11, lineBuffer, 8);
#ifdef DEBUG
      Serial.printf("FL: date time from logfile line: %s\n", timeStampCurrentLogEntry);
#endif
      DateTime dateTimeCurrent = DateTime(timeStampCurrentLogEntry);
      if (!dateTimeCurrent.isValid()) {
#ifdef DEBUG
        Serial.printf("FL: Invalid!! - will skip :> %s ", lineBuffer);
#endif
        continue;
      }
#ifdef DEBUG
        Serial.printf("FL: dateTimeCurrent is valid!\n");
#endif
      if (timestampOffset != nullptr) {
        // compare the /time/ of the current record with the offset
        // and maybe skip this record if it's been recored not later
        // than last uploaded record (from the day of this logfile)
        // at the last upload
        char timeCurrentLogEntry[9];
        memcpy(timeCurrentLogEntry, lineBuffer, 8);
        timeCurrentLogEntry[8] = 0;
        if(TimeCompare(timeCurrentLogEntry, timestampOffset) <= 0) {
#ifdef DEBUG_TIME_COMPARE
          Serial.printf("FL: current: %s <= offset: %s? -> Skip older!\n", timeCurrentLogEntry, timestampOffset);
#endif
          continue;
        }
      }

      timedRecord.unixtime = dateTimeCurrent.unixtime();
      timedRecord.air = &airQualityRecord;

      callbackFunction(&timedRecord, callbackParameter);
    // Todo: make sure, timeStampLastRecordSend is overwritten only if
    // parsing and sending was successful!lastTimeARecordWasSendAt
      memcpy(timestampOfLastRecordSent, lineBuffer, 8);
      recordsUploaded++;

    }
  }
  logFile.close();
  return recordsUploaded;
}

/**
 * Parse root ("/") of sd card for up to $maxFiles valid logfiles into the filebuffer
 * Filter those older than date of last record sent
 */
uint8_t FileLogger::parseRootForUnprocessedLogFiles(char* fileNameBuffer[],
                          uint8_t maxFiles, const char* dateOfLastRecordSent) {

  file_t root;
  if (!root.open("/")) {
    Serial.print("WARN FL: error open root\n");
    return 0;
  }

  file_t currentFile;
  char currentFileName[20];
  char currentFileDateBuffer[11];
  bool validPreviousUploadDetected = false;
  DateTime dateLastRecordSend;
  if (dateOfLastRecordSent != nullptr) {
    dateLastRecordSend = DateTime(dateOfLastRecordSent);
    validPreviousUploadDetected = dateLastRecordSend.isValid();
  }
  uint8_t fileCounter = 0;

  // loop through all files found on the root of the SD card
  // and filter the ones matching the log file pattern.
  while (currentFile.openNext(&root, O_RDONLY)) {

    if (!currentFile.isHidden() && !currentFile.isDir()) {
      currentFile.getName(currentFileName, 20);
      currentFile.close();

      // if the name matches the pattern of the logfile
      if(getDateOfLogFile(currentFileName, currentFileDateBuffer) != nullptr) {
#ifdef DEBUG_DIR
        Serial.printf("FL: Found logfile with %s\n", currentFileDateBuffer);
#endif
        DateTime currentFileDate(currentFileDateBuffer);
        if(!currentFileDate.isValid()) {
          // The file found doesn't start with a valid
          // date e.g. 2020-12-12
          // skipping it.
          continue;
        }

        if (validPreviousUploadDetected) {
          // verify the date of the current logfile is
          // more recent or from the same day of the previous upload
          // date of last upload is not older than the the date of the current file
          if (dateLastRecordSend > currentFileDate) {
#ifdef DEBUG_DIR
            Serial.printf("FL: %s > %s ?? =>> skipping this logfile\n", dateOfLastRecordSent, currentFileDateBuffer);
#endif
            continue;
          }
        }
        // store the date part (prefix) of the logfiled discovered
        // for processing.
        fileNameBuffer[fileCounter] = (char*) malloc(11);
        memcpy(fileNameBuffer[fileCounter], currentFileDateBuffer, 10);
        fileNameBuffer[fileCounter][10] = '\0';

        if (++fileCounter >= maxFiles) {
          break;
        }
      }
    } else {
      currentFile.close();
    }
  }
  root.close();
  return fileCounter;
}

/**
 * Verify it's a valid logfile name with currect length
 * and ending '_air.log'.
 * @param name - the name to be checked
 * @param date - a buffer for file names's date part to be copied into
 *               needs to have 11 bytes minimal length
 * @return pointer to the date buffer if valid, nullptr otherwise
 */
char* FileLogger::getDateOfLogFile(const char *name, char* date) {

  if (name == nullptr || date == nullptr) {
#ifdef DEBUG
    Serial.printf("WARN FL: name(%s) date(%s) :\n", name, date);
#endif
    return nullptr;
  }
#ifdef DEBUG_DIR
  Serial.printf("FL: Checking file with name: %s :\n", name);
#endif
  if (strnlen(name,19) != 18){
#ifdef DEBUG_DIR
    Serial.printf("Name to %s :\n", (strlen(name) < 18)? "short" : "long");
#endif
    return nullptr;
  }
  if( strncmp("_air.log", name+10, 8) != 0) {
#ifdef DEBUG_DIR
    Serial.print("Wrong postfix\n");
#endif
    return nullptr;
  }
  memcpy(date, name, 10);
  date[10] = 0;
#ifdef DEBUG_DIR
  Serial.printf("Date time of file: %s \n", date);
#endif
  return date;
}

/**
 * Parse a line of the logfile into a AirQualityRecord
 *
 * @param record - Pointer to the record to store the values in
 * @returns true if a data for a valid record could be parsed
 */
bool FileLogger::parseLogFileLine(char* lineBuffer, AirQualityRecord* record) {
  char * strtokIndx; // this is used by strtok() as an index

  strtokIndx = strtok(lineBuffer,","); // get the first part (the time but ignore)
  if (strtokIndx == nullptr) {
#ifdef DEBUG
    Serial.print("No one comma in line\n");
#endif
    return false;
  }

  strtokIndx = strtok(NULL, ",");
  if (strtokIndx == nullptr) {
#ifdef DEBUG
    Serial.print("Only one comma in line\n");
#endif
    return false;
  }
  record->temperature = atof(strtokIndx);

  strtokIndx = strtok(NULL, ",");
  if (strtokIndx == nullptr) {
#ifdef DEBUG
    Serial.print("Second value (humidity?) missing\n");
#endif
    return false;
  }
  record->humidity = atof(strtokIndx);

  strtokIndx = strtok(NULL, ",");
  if (strtokIndx == nullptr){
#ifdef DEBUG
    Serial.print("Third value (co2?) missing\n");
#endif
    return false;
  }
  record->co2 = atoi(strtokIndx);
  // if airrecord == valid
  if (record->humidity > 0.0F && record->humidity <= 100.0F
      && record->temperature > -65.5F && record->temperature < 90.0F
      && record->co2 > 100 && record->co2 < 4800
  ) {
    return true;
  }
#ifdef DEBUG
  Serial.print("Values not in range - not valid\n");
#endif
  return false;
}

/**
 * https://www.espressif.com/sites/default/files/2c-esp8266_non_os_sdk_api_guide_en_v1.5.4.pdf
 *
 * Data read/write accesses to the RTC memory must be word aligned (4 bytes boundary aligned).
 * Parameter des_addr means block number(4 bytes per block).
 * For example, to save data at the beginning of user data area, des_addr will be 256/4 = 64,
 * save_size will be data length in bytes
 */
bool FileLogger:: writeRecordToRtcUserMemory(uint32_t offset, RtcMemAirLogRecord *record)
{
  // targetbucket calculation:
  // rtcMemOffsetStorePostion e.g. 65 (using for storing offset)
  // start storing record date from next bucket (+1)
  // offset number of data record (each record takes 8 bytes => 2 buckets)
  //  ==> offset * 2

  // for each record 8 bytes are needed
  uint8 targetBucket = rtcMemOffsetStorePostion + 1 + offset*2;


  // User memory 512 bits -> 128 buckets (a 4 bytes)
  // 2 buckets per record -> last possible target buckets 127+128
  if (targetBucket < 128U) {
    uint64_t data = 0;

    // data stored in RTC user mem seems to be stored
    // reliable only if of datatype uint32_t
    // at least, storing RtcMemAirLogRecord directly didn't work
    // The reason might be that it takes more than 3x32 internally.
    // Packing unsigned humidity and signed temperature
    // with factor 10 to store float value with 1 fraction precision
    // as int using 12 bit each, the complete log can be stored
    // using 2 bucket per record instead of 3.
    data |= record->hour;
    data <<= 8;
    data |= record->minute;
    data <<= 8;
    data |= record->second;
    // use 12 bit of the 16 bit temperature value only
    data <<= 12;
    // use msb for storing info if it's a negative value
    // and store absolute value
    if (record->temperature < 0) {
      data |= (record->temperature * -1);
      data |= 0x800;
    } else {
      data |= (record->temperature & 0x7FF);
    }
    // use 12 bit of the 16 bit humidity value only
    data <<= 12;
    data |= record->humidity;
    data <<= 16;
    data |= record->co2;

#ifdef DEBUG_RTC_MEM
  printLogRecord(record);
  Serial.print("FL: Write Data (");
  printUint64(data);
  Serial.printf(") to bucket: %u\n", targetBucket);
#endif

    return system_rtc_mem_write(targetBucket, &data, BYTES_PER_RECORD);

  }
  return false;
}

/**
 * For Documentation about buckets and sizes and calculations
 * @see writeRecordToRtcUserMemory() above
 */
bool FileLogger::readRecordFromRtcUserMemory(uint32_t offset, RtcMemAirLogRecord *record) {

  uint8 sourceBucket = rtcMemOffsetStorePostion + 1 + offset*2;

  if (sourceBucket < 128U) {

    uint64_t data = 0;

    if (system_rtc_mem_read(sourceBucket, &data, BYTES_PER_RECORD)) {
#ifdef DEBUG_RTC_MEM
      Serial.print("Read Data (");
      printUint64(data);
      Serial.printf(") from bucket: %u\n", sourceBucket);
#endif

      record->co2 = data & 0xFFFF;
      data >>= 16;

      // humitidy stored using 12 bit
      record->humidity = data & 0x0FFF;
      data >>= 12;

      // humidity stored using 12 bit
      // msb is indicating negative value
      record->temperature = data & 0x07FF;
      if(data & 0x800) // sign bit set?
        record->temperature *= -1;
      data >>= 12;

      record->second = data & 0xFF;
      data >>= 8;
      record->minute = data & 0xFF;
      data >>= 8;
      record->hour = data & 0xFF;
      return true;

    }
  }

  return false;
}


uint16_t FileLogger::getLastLoggedMinute() {
  uint32_t rtcMemMultiStorageWord;
  readRtcMemMultiStorageWord(&rtcMemMultiStorageWord);
#ifdef DEBUG_FULL
  Serial.printf("Last logged minute: %u\n", (rtcMemMultiStorageWord >> 16 ) & 0xFFFF);
#endif
  return (rtcMemMultiStorageWord >> 16) & 0xFFFF;

}

void FileLogger::setLastLoggedMinute(uint16_t minute) {
  uint32_t rtcMemMultiStorageWord;
  readRtcMemMultiStorageWord(&rtcMemMultiStorageWord);
  rtcMemMultiStorageWord &= 0x0000FFFF;
  rtcMemMultiStorageWord |= (minute << 16);

  writeRtcMemMultiStorageWord(&rtcMemMultiStorageWord);
}

uint16_t FileLogger::getRtcMemDataRecordOffset() {
  uint32_t rtcMemMultiStorageWord;
  readRtcMemMultiStorageWord(&rtcMemMultiStorageWord);

  return rtcMemMultiStorageWord & 0xFFFF;
}

void FileLogger::setRtcMemDataRecordOffset(uint16_t offset) {
  uint32_t rtcMemMultiStorageWord;
  readRtcMemMultiStorageWord(&rtcMemMultiStorageWord);
  rtcMemMultiStorageWord &= 0xFFFF0000;
  rtcMemMultiStorageWord |= offset;
  writeRtcMemMultiStorageWord(&rtcMemMultiStorageWord);
}

void FileLogger::readRtcMemMultiStorageWord(uint32_t *rtcMemMultiStorageWord) {
  ESP.rtcUserMemoryRead(rtcMemOffsetStorePostion, rtcMemMultiStorageWord, 1);
}

void FileLogger::writeRtcMemMultiStorageWord(uint32_t *rtcMemMultiStorageWord) {
#ifdef DEBUG_RTC_MEM
  Serial.printf("Write multi storage: ");
  printUint32(*rtcMemMultiStorageWord);
  Serial.print("\n");
#endif
  ESP.rtcUserMemoryWrite(rtcMemOffsetStorePostion, rtcMemMultiStorageWord, 1);
}

bool FileLogger::waitForSDCardReady() {
    uint8_t wait = 0;
  // Wait until SD is not busy.
  while (sd.card()->isBusy()) {
    delay(100);
    yield();
    wait++;
    if (wait > 20) {
#ifdef DEBUG
      Serial.print("Giving up waiting for SD card not busy");
#endif
      return false;
    }
  }
  return true;
}

/**
 *  D E B U G - F U N C T I O N S
 *
 */
#ifdef DEBUG
void FileLogger::printAirQualityRecord(AirQualityRecord *record) {

  Serial.print(record->temperature, 1);
  Serial.print(", ");
  Serial.print(record->humidity, 1);
  Serial.print(", ");
  Serial.printf("%4u\n", record->co2);

}

void FileLogger::printLogRecord(RtcMemAirLogRecord *record) {

  Serial.printf("FL: %02u:%02u:%02u, ", record->hour, record->minute, record->second);
  Serial.print(record->temperature / 10.0F, 1);
  Serial.print(", ");
  Serial.print(record->humidity / 10.0F, 1);
  Serial.print(", ");
  Serial.printf("%4u\n", record->co2);

}

void FileLogger::printUint64(uint64_t data) {

  for (int8_t b=3; b>=0; b--) {
      uint16_t dataWord = (data >> b*16 )& 0xFFFF;
      Serial.printf("%04x ", dataWord);
  }
}
void FileLogger::printUint32(uint32_t data) {

  for (int8_t b=1; b>=0; b--) {
      uint16_t dataWord = (data >> b*16 )& 0xFFFF;
      Serial.printf("%04x ", dataWord);
  }
}
#endif
