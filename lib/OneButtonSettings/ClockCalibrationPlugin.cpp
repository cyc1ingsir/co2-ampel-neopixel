#include "ClockCalibrationPlugin.h"

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <Arduino.h> // for serial

#include <ezTime.h>
#include <RTClib.h>

ClockCalibrationPlugin::ClockCalibrationPlugin(SettingsDisplay* display)
  : OneButtonSettingsPlugin(display) {
}

void ClockCalibrationPlugin::tick() {}

bool ClockCalibrationPlugin::click() {
#ifdef DEBUG
  Serial.print("Start Calibration \n");
#endif
  displayMenuEntry(PLUGIN_RUNNING);
  syncOnceWithNTP();
  return true; // change to false once the functionality is implemented
}

void ClockCalibrationPlugin::doubleClick() {
  Serial.print("Calibration Plugin doubleClick \n");

}

void ClockCalibrationPlugin::longPressedStop() {

}

bool ClockCalibrationPlugin::longPressedStart() {
  return false;
}

void ClockCalibrationPlugin::displayMenuEntry(pluginState_t state) {
  m_display->display('O', state);
}


bool ClockCalibrationPlugin::syncOnceWithNTP() {
  if (!connectToWiFi()) {
    error = true;
    return false;
  }
  setServer("de.pool.ntp.org");
  setInterval();
  updateNTP();
  if(!waitForSync(30)) {
    Serial.print(errorString());
    Serial.print("\n");
    error = true;
    return false;
  }
  RTC_DS3231 rtc;
  DateTime current(UTC.now());
  rtc.adjust(current);
  error = false;
#ifdef DEBUG
  Serial.printf("CC-Plugin: Time set to UTC: %u:%u:%u", current.hour(), current.minute(), current.second());
#endif
  return true;
}

bool ClockCalibrationPlugin::connectToWiFi() {
  // this will wake up WiFi even if device slept with WAKE_RF_DISABLED
  // tip from https://github.com/esp8266/Arduino/issues/3072 (and bakke.online)
  if (!WiFi.isConnected()) {
    WiFi.persistent(true);
    WiFi.forceSleepBegin();
    delay(10);
    WiFi.persistent(false);
    WiFi.forceSleepWake();
    delay(10);
    WiFi.mode(WIFI_STA);
    WiFi.begin();
    delay(10);

  uint16_t counter = 0;
    while (!WiFi.isConnected()) {
      delay(250);
      yield();
      counter++;
      if (counter > 40){
        Serial.print("WARN Calibration: Connection to WiFi not successful!! \n");
        return false;
      }
    }
  }
  return true;
}
