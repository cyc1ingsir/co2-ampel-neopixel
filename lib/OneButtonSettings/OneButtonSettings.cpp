#include "OneButtonSettings.h"

#include "UploadPlugin.h"
#include "CalibrationPlugin.h"
#include "ClockCalibrationPlugin.h"
#include "NeoPixelSettingsDisplay.h"

/**
 * Callback functions for OneButton
 * can't be member functions.
 */
void settingsClickFunction(void* obs) {
  ((OneButtonSettings*)obs)->click();
}

void settingsDoubleClickFunction(void* obs) {
  ((OneButtonSettings*)obs)->doubleClick();
}

void settingsLongPressedStop(void* obs) {
  ((OneButtonSettings*)obs)->longPressedStop();
}

void settingsLongPressedStart(void* obs) {
  ((OneButtonSettings*)obs)->longPressedStart();
}
// End of callback functions


OneButtonSettings::OneButtonSettings(int pin, FileLogger* fileLogger,
  boolean activeLow, bool pullupActive)
    : OneButton(pin, activeLow, pullupActive)
    , functionState(SETUP_IDLE)
    , activePlugin(0)
    , m_pin(pin)
    , m_display(new NeoPixelSettingsDisplay()) {
  activePluginPtr[0] = new UploadPlugin(m_display, fileLogger);
  activePluginPtr[1] = new CalibrationPlugin(m_display);
  activePluginPtr[2] = new ClockCalibrationPlugin(m_display);
}

void OneButtonSettings::begin() {

// if pin is RX pin it needs to be updated
// since Serial.begin will set it to LOW
  if (m_pin == 3 ) {
    pinMode(m_pin, INPUT_PULLUP);
  }
}

/**
 * Attaches the callbacks
 * as they might be attached to a different operation
 * during normal run mode.
 */
void OneButtonSettings::startSetup() {
//D Serial.print("OneButtonSettings: starting Setup \n");
  attachClick(settingsClickFunction, this);

  attachDoubleClick(settingsDoubleClickFunction, this);

  attachLongPressStart(settingsLongPressedStart, this);

  attachLongPressStop(settingsLongPressedStop, this);

  // indicate that setup mode is active
  m_display->displayCross();
}

void OneButtonSettings::endSetup() {
  attachClick(nullptr, nullptr);

  attachDoubleClick(nullptr, nullptr);

  attachLongPressStart(nullptr, nullptr);

  attachLongPressStop(nullptr, nullptr);
}

void OneButtonSettings::loop() {
    tick();
    if(functionState == SETUP_IN_PLUGIN_ACTIVE) {
      activePluginPtr[activePlugin]->tick();
    }
}

void OneButtonSettings::click() {
#ifdef DEBUG_STATE
  printCurrentFunctionState("click IN");
#endif
  switch (functionState) {
    case SETUP_STARTED:
    case SETUP_IN_PLUGIN: // fallthrough on purpose
      setNextActivePlugin();
      break;
    case SETUP_IN_PLUGIN_ACTIVE:
      if(activePluginPtr[activePlugin]->click()) {
        // (one of) function completed by plugin
        // will deactivate this one again.
        functionState = SETUP_IN_PLUGIN;
        activePluginPtr[activePlugin]
          ->displayMenuEntry((activePluginPtr[activePlugin]->hasHadError())? PLUGIN_ERROR : PLUGIN_SELECTED);
      }
      break;
    default:
      break;
  }
#ifdef DEBUG_STATE
  printCurrentFunctionState("click OUT");
#endif
}

void OneButtonSettings::doubleClick() {
  if(functionState == SETUP_IN_PLUGIN_ACTIVE)
    activePluginPtr[activePlugin]->doubleClick();
#ifdef DEBUG
  Serial.print("Setup: Detected double click\n");
#endif
}

void OneButtonSettings::longPressedStop() {
  if(functionState == SETUP_IDLE) {
    functionState = SETUP_STARTED;
//D    Serial.print("longPressStop: Setup mode started!\n");

  } else {
#ifdef DEBUG_STATE
    printCurrentFunctionState("longPressStop");
#endif
  }
}

bool OneButtonSettings::longPressedStart() {
#ifdef DEBUG_STATE
  printCurrentFunctionState("long press start");
#endif
  switch (functionState) {
    case SETUP_STARTED:
      // leave the setup entirely
      functionState = SETUP_FINISHED;
      endSetup();
      m_display->clear();
      break;
    case SETUP_IN_PLUGIN:
      // activate setings mode for the
      // currently selected plugin
      functionState = SETUP_IN_PLUGIN_ACTIVE;
      activePluginPtr[activePlugin]->displayMenuEntry(PLUGIN_ACTIVE);
#ifdef DEBUG_STATE
      Serial.print("longPressedStart() - CASE: IN_PLUGIN\n");
#endif
      break;
    case SETUP_IN_PLUGIN_ACTIVE:
      if(!activePluginPtr[activePlugin]->longPressedStart()) {
        // if not handled by the plugin itself
        // this will end the settings mode of the
        // currently selected plugin
        functionState = SETUP_IN_PLUGIN;
#ifdef DEBUG_STATE
        Serial.print("longPressedStart() - CASE: PLUGIN_ACTIVE (not handled by plugin)\n");
#endif
        activePluginPtr[activePlugin]->displayMenuEntry();
      }
      break;
    case SETUP_IDLE: // fallthrough - do nothing
    default:
#ifdef DEBUG_STATE
      Serial.print("LongPressedStart: Not handled state\n");
#endif
      break;
  }
  return true;
}

/**
 * Select the next settings plugin.
 */
void OneButtonSettings::setNextActivePlugin() {
  if(functionState == SETUP_IN_PLUGIN) {
    activePlugin++;
  }
  if(activePlugin >= NUMBER_OF_REGISTERED_PLUGINS) {
    activePlugin = 0;
    functionState = SETUP_STARTED;
    m_display->displayCross();
    return;
  }
  functionState = SETUP_IN_PLUGIN;
  activePluginPtr[activePlugin]->displayMenuEntry();
}

#ifdef DEBUG_STATE
/*
 * For debugging purpose only
 */
void OneButtonSettings::printCurrentFunctionState(const char* prefix) {
  Serial.printf("Current state (%s): ", prefix);
  switch (functionState) {
    case SETUP_IDLE:
      Serial.print("SETUP_IDLE");
      break;
    case SETUP_STARTED:
      Serial.print("SETUP_STARTED");
      break;
    case SETUP_IN_PLUGIN:
      Serial.print("SETUP_IN_PLUGIN");
      Serial.printf(" - Plugin# (%u)", activePlugin);
      break;
    case SETUP_IN_PLUGIN_ACTIVE:
      Serial.print("SETUP_IN_PLUGIN_ACTIVE");
      Serial.printf(" - Plugin# (%u) !ACTIVE!", activePlugin);
      break;
    case SETUP_FINISHED:
      Serial.print("SETUP_FINISHED");
      break;
    default:
      Serial.print("UNKNOWN");
      break;
  };
  Serial.print("\n");
}
#endif
