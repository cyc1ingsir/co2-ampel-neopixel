// this is needed for querying the reset reason
#include "user_interface.h"

#include "neopixel_gauge.h"


NeoPixel_Gauge::NeoPixel_Gauge(uint16_t resolution, Direction_t direction,
  uint16_t numPixel, uint16_t pin, neoPixelType type = NEO_GRB + NEO_KHZ800)
    : Adafruit_NeoPixel(numPixel, pin, type)
    , resolution(resolution)
    , direction(direction)
    , last_value(0) {
      // initialize last_value store in rtc mem after powering up
      // but use it only if be woken up from deep sleep since the value
      // right after power up is undefined.
      if(ESP.getResetInfoPtr()->reason != REASON_DEEP_SLEEP_AWAKE) {
        ESP.rtcUserMemoryWrite(LAST_VALUE_RTC_MEM_OFFSET, &last_value, 1);
      }
}


void NeoPixel_Gauge::displayValue(uint16_t val){
  uint8_t value = val / resolution;

  ESP.rtcUserMemoryRead(LAST_VALUE_RTC_MEM_OFFSET, &last_value, 1);
#ifdef DEBUG
  Serial.printf("display value %u -> %u pixel by resolution: %u from (%u)\n", val, value, resolution, last_value);
#endif
  if (value == last_value) {
    // refresh anyway in case something else was being displayed in the meantime
    showValue(value);
  } else if (value > numLEDs && last_value > numLEDs) {
    value = numLEDs + 1;
    showValue(value);
    last_value = value;
  } else {
    while (last_value != value) {
      // smoothing transition between displaying
      // two different values
      // prevents 'pointer jumping'
      if(last_value < value) {
        last_value++;
      } else {
        last_value--;
      }

      showValue(last_value);
      if (last_value > numLEDs)
        break;
      delay(20* abs(last_value - value));
    }
  }

  // store last value displayed in rtc ram to survive deep sleep
  ESP.rtcUserMemoryWrite(LAST_VALUE_RTC_MEM_OFFSET, &last_value, 1);
#ifdef DEBUG
  Serial.printf("last value stored: (%u)\n", last_value);
#endif

}


// private
void NeoPixel_Gauge::showValue(uint8_t val){

  if (val > numLEDs) {
    // indicate overflow with an extra bright purpelish colour
    // at max pixel (direction ommited)
    setPixelColor(numLEDs-1, 0x200609);
    for (int i=0; i < (numLEDs -1); i++) {
      // and delete all others
      setPixelColor(i,0,0,0);
    }
    show();
    return;
  }

  int i; // pixel to set

  for (int k=0; k < numLEDs; k++) { // iterate over all pixel

    // clockwise or opposite
    if (direction==CLOCKWISE) {
      i=k;
    } else {
      i=numLEDs-1-k;
      if (i == numLEDs-1) i=0;
      else i=i+1;
    }

    if (k+1 == val) {
      setPixelColor(i, _GaugeScaleColoursBright[i]);
    } else if (k+1 <= val && k+1 > val-MAX_POINTER_WIDTH) {
      setPixelColor(i, _GaugeScaleColours[i]);
    } else { // switch off pixel
      setPixelColor(i,0,0,0);
    }
  }
  show(); // display value
}

void NeoPixel_Gauge::displayTime(DateTime* dateTime) {
  // 12 => OFFSET to adjust the orientation of the pixel ring
  // i.e. wich pixel is south (6o'clock), with is north (12o'clock)
  // ToDo - consider timezone!
  uint8_t hour = dateTime->twelveHour() + 1; // MEZ
  uint8_t hourPixel = (hour * 2 + 12) % 24;
  uint8_t minute = dateTime->minute();
  if (minute > 30) hourPixel++;
  // loosing precision due to the fact, hat we have 24 pixels for 60 minutes
  uint8_t minutePixel = (static_cast<int>(minute / 2.5) + 12) % 24;
//DEBUG  // Serial.printf("Set hour(%u) pixel to: %u\n", hour, hourPixel);
//DEBUG  // Serial.printf("Set minute(%u) pixel to: %u\n", minute, minutePixel);

//DEBUG  Serial.println(dateTime->toString(TIMESTAMP_FULL));

  if (hourPixel == minutePixel) {
    setPixelColor(hourPixel, 0x020609);
  } else {
    setPixelColor(hourPixel, 0x020308);
    setPixelColor(minutePixel, 0x020906);
  }
  show();
    if (last_value != 0) {
    // limit time the time of day is being displayed
    delay(5000);
    // and reset to displaying the last value
    showValue(last_value);
  }

}

void NeoPixel_Gauge::displayGreenCross() {
  clear();
  setPixelColor(6, 0x000a00);
  setPixelColor(12, 0x000a00);
  setPixelColor(18, 0x000a00);
  setPixelColor(0, 0x000a00);
  show();
}

void NeoPixel_Gauge::swipeClean() {
  // this doesn't adhere to direction
  // needed some work if pixel ring is anti clockwise
  // first swipe right (one pixel at a time)
  for(uint8_t n=0; n<numLEDs; n++) {
      setPixelColor(n,0x000a0a10);
      if(n>0) {
          setPixelColor(n-1,0x00);
      }
      show();
      delay(n+5);
  }
  // swipe left afterwards
  for(uint8_t n=numLEDs; n>0; n--) {
      setPixelColor(n-1,0x000a0a10);
      if(n<numLEDs) {
          setPixelColor(n,0x00);
      }
      show();
      delay(n+2);
  }
  setPixelColor(0,0x00);
  show();

}
