#ifndef FILELOGGER_H
#define FILELOGGER_H

/**
 * Define methods and structures to log
 * air quality data to a file on a SD memory card
 * using the SdFat library from
 * https://github.com/greimann/SdFat
 *
 */

#include "SdFat.h"
#include "sdios.h"
#include "RTClib.h"
#include "airDataTypes.h"

#define DEBUG true
#define DEBUG_RTC_MEM true

#define DATA_RECORDS_RTC_MEM_POSTION 65
#define BYTES_PER_RECORD 8U

#define LAST_RECORD_SENT_FILE "lastSendRecord.txt"

// standard CS pin on Wemos D1 min pro is GPIO 15
#ifndef SDCARD_SS_PIN
const uint8_t SD_CS_PIN = SS;
#else  // SDCARD_SS_PIN
const uint8_t SD_CS_PIN = SDCARD_SS_PIN;
#endif  // SDCARD_SS_PIN


// Try to select the best SD card configuration.
// SD_CONFIG is used in begin()
#if HAS_SDIO_CLASS
#define SD_CONFIG SdioConfig(FIFO_SDIO)
#elif ENABLE_DEDICATED_SPI
#define SD_CONFIG SdSpiConfig(SD_CS_PIN, DEDICATED_SPI, SD_SCK_MHZ(16))
#else  // HAS_SDIO_CLASS
#define SD_CONFIG SdSpiConfig(SD_CS_PIN, SHARED_SPI, SD_SCK_MHZ(16))
#endif  // HAS_SDIO_CLASS

// using a 4GB ySD card formated with the formating sketch
// to Fat32
#define SD_FAT_TYPE 1

#if SD_FAT_TYPE == 0
typedef SdFat sd_t;
typedef File file_t;
#elif SD_FAT_TYPE == 1
typedef SdFat32 sd_t;
typedef File32 file_t;
#elif SD_FAT_TYPE == 2
typedef SdExFat sd_t;
typedef ExFile file_t;
#elif SD_FAT_TYPE == 3
typedef SdFs sd_t;
typedef FsFile file_t;
#else  // SD_FAT_TYPE
#error Invalid SD_FAT_TYPE
#endif  // SD_FAT_TYPE

// used internally with the methods
// storing the data temporarily
// in the RTC user data memory to
// survive deep sleep
typedef struct {
  uint8_t second;
  uint8_t minute;
  uint8_t hour;
  int16_t temperature; // x 10: 19.8'C -> 198
  uint16_t humidity;    // Humidity with 1 fraction x 10
  uint16_t co2;
} RtcMemAirLogRecord;

// 2020-12-02T19:30:00, 4000, -55.0, 100
#define MAX_LOGENTRY_LENGHT 45
#define MAX_LOGFILES 20

extern "C" {
typedef bool (*recordCallbackFunction)(TimedAirQualityRecord* record, void*);
}

class FileLogger {

  public:
    FileLogger(uint8_t rtcMemOffsetStorePostion = DATA_RECORDS_RTC_MEM_POSTION);
    ~FileLogger();

    /**
     * Initialisation mainly of the SD library
     */
    bool begin();

    /**
     * Add a data point which will be added to
     * the RTC user memory
     * This limits the maximum ammount of records to be
     * held in memory before writing them to the SD card
     * This is a tradeoff between writing to the logfile to
     * frequently (consuming energy) and the possible loss of
     * records caused by powering the device off.
     * See documention of writeRecordToRtcUserMemory - method
     * on calculating the maximum of records possible stored
     */
    void addDataPoint(DateTime *dateTime, AirQualityRecord *record);

    /*
     * This will write/append all stored data records to a file with
     * the name YYYY-MM-DD_air.log to the root of the SD card
     * (year, month, day is used from the DateTime provided)
     * The offset for the data buckets is reset to 0
     */
    bool writeValuesToFile(DateTime *dateTime);

    /**
     * Retriev all records which not have been retrieved before, calling the
     * callback function for every record.
     * @param parameter Will be passed to the callback function (could be e.g. the caller)
     */
    bool retrieveData(recordCallbackFunction callbackFunction, void* parameter);

    /*
     * Get the minute value of the last stored record. No matter if
     * stored within the RTC memory or already persisted to the logfile.
     * This is useful for recording date every 2 minutes whilst the
     * sensor is being queried more frequently e.g. every 30 seconds.
     * The value returned will be survive deep sleep.
     */
    uint16_t getLastLoggedMinute();

  private:

    uint8_t parseRootForUnprocessedLogFiles(char* fileNameBuffer[],
                          uint8_t maxFiles, const char* lastDateARecordWasSendAt = nullptr);
    uint16_t processLogFile(const char* date, char* timestampOfLastRecordSent, const char* timestampOffset,
                          recordCallbackFunction callbackFunction, void* callbackParameter);
    char* getDateOfLogFile(const char *name, char* dateTime);
    bool parseLogFileLine(char* lineBuffer, AirQualityRecord* record);

    void writeRecordToLogFile(file_t *logFile, RtcMemAirLogRecord *record);

    // functions dealing with storing infromation/records inside the
    // RTC memory
    bool writeRecordToRtcUserMemory(uint32_t offset, RtcMemAirLogRecord *record);
    bool readRecordFromRtcUserMemory(uint32_t offset, RtcMemAirLogRecord *record);

    uint16_t getRtcMemDataRecordOffset();
    void setRtcMemDataRecordOffset(uint16_t offset);
    void setLastLoggedMinute(uint16_t minute);

    void readRtcMemMultiStorageWord(uint32_t *rtcMemMultiStorageWord);
    void writeRtcMemMultiStorageWord(uint32_t *rtcMemMultiStorageWord);

    bool waitForSDCardReady();

    // functions for debugging
#ifdef DEBUG
    void printAirQualityRecord(AirQualityRecord *record);
    void printLogRecord(RtcMemAirLogRecord *record);
    void printUint64(uint64_t data);
    void printUint32(uint32_t data);
#endif

    // member
    sd_t sd;        // see #define SD_FAT_TYPE above

    // bucket in witch the current record (and the last logged minute) is stored
    // within the RTC user memory
    uint8_t rtcMemOffsetStorePostion;

};

#endif // FILELOGGER_H
