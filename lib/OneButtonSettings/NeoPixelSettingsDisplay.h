#ifndef NEOPIXEL_SETTINGS_DISPLAY_H
#define NEOPIXEL_SETTINGS_DISPLAY_H

#include "SettingsDisplay.h"

#include <Adafruit_NeoPixel.h>

/**
 * Settings Display using a 24 pixel NeoPixel ring.
 */
class NeoPixelSettingsDisplay : public SettingsDisplay {

public:
  NeoPixelSettingsDisplay();

  void display(unsigned short number) override;

  void display(const char c, pluginState_t state) override;

  void displayCross() override;

  void clear() override;

  private:
    Adafruit_NeoPixel* m_neoPixel;
};

#endif // NEOPIXEL_SETTINGS_DISPLAY_H
