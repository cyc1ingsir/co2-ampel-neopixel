#ifndef ONE_BUTTON_SETTINGS_H
#define ONE_BUTTON_SETTINGS_H

#include <OneButton.h>
#include <FileLogger.h>

#include "OneButtonSettingsPlugin.h"

enum SettingsFunction {
  SETUP_IDLE,
  SETUP_STARTED,
  SETUP_IN_PLUGIN,
  SETUP_IN_PLUGIN_ACTIVE,
  SETUP_FINISHED
};

#define NUMBER_OF_REGISTERED_PLUGINS 3

/**
 * Declaration of
 * callback functions for OneButton
 * can't be member functions.
 */
void settingsClickFunction(void* obs);

void settingsDoubleClickFunction(void* obs);

void settingsLongPressedStop(void* obs);
// End of callback functions declaration

class OneButtonSettings : public OneButton {

  public:

    OneButtonSettings(int pin, FileLogger* fileLogger,
      boolean activeLow = true, bool pullupActive = true);

    void begin();

    void startSetup();

    void loop();

    bool isPressed() { return getPressedTicks() != 0; }

    bool isSetupFinished() { return functionState == SETUP_FINISHED;}

    void clearFinished() { functionState = SETUP_IDLE;}

    void click();

    void doubleClick();

    void longPressedStop();

    bool longPressedStart();

  private:

    SettingsFunction functionState;
    void printCurrentFunctionState(const char* prefix);

    void endSetup();

    void setNextActivePlugin();
    uint8_t activePlugin;
    OneButtonSettingsPlugin* activePluginPtr[NUMBER_OF_REGISTERED_PLUGINS];
    uint8_t m_pin;

    SettingsDisplay* m_display;

};

#endif // ONE_BUTTON_SETTINGS_H
