#ifndef FILELOGGER_CALLBACK_H
#define FILELOGGER_CALLBACK_H

#include <RTClib.h>
#include <SdFat.h>

// Call back for file timestamps.  Only called for file create and sync().
void dateTimeCallback(uint16_t* date, uint16_t* time, uint8_t* ms10) {

  RTC_DS3231 rtc;
  DateTime now = rtc.now();

  // Return date using FS_DATE macro to format fields.
  *date = FS_DATE(now.year(), now.month(), now.day());

  // Return time using FS_TIME macro to format fields.
  *time = FS_TIME(now.hour(), now.minute(), now.second());

  // Return low time bits in units of 10 ms.
  *ms10 = now.second() & 1 ? 100 : 0;
}

#endif // FILELOGGER_CALLBACK_H
