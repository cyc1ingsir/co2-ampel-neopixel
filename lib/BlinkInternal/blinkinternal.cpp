#include "blinkinternal.h"

/**
 * Use onboard LED to indicate some (error) states
 */
void blinkInternal(uint8_t times, unsigned long duration, unsigned long on) {
  for (uint8_t b=0; b < times; b++) {
    yield();
    digitalWrite(LED_BUILTIN, LOW);
    delay((on > 0)? on : duration);
    digitalWrite(LED_BUILTIN, HIGH);
    delay(duration);
    yield();
  }
}
