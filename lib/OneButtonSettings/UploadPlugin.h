#ifndef UPLOAD_PLUGIN_H
#define UPLOAD_PLUGIN_H

#include "OneButtonSettingsPlugin.h"

#include <FileLogger.h>
#include "InfluxDBAirDataUploader.h"

class UploadPlugin : public OneButtonSettingsPlugin {

public:
  UploadPlugin(SettingsDisplay* display, FileLogger* fileLogger);

  void tick() override;

  bool click() override;

  bool uploadRecord(TimedAirQualityRecord* record);

  void doubleClick() override;

  void longPressedStop() override;

  bool longPressedStart() override;

  void displayMenuEntry(pluginState_t state) override;

private:
  FileLogger* m_fileLogger;
  InfluxDBAirDataUploader* m_airDataUploader;
};

#endif // UPLOAD_PLUGIN_H
