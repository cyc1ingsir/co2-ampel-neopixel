#include <Arduino.h>
#include <FileLogger.h>

#include <SparkFun_SCD30_Arduino_Library.h>
#include <Wire.h>
#include <ESP8266WiFi.h>

#include <OneButtonSettings.h>
#include <blinkinternal.h>
#include "neopixel_gauge.h"

/*
 * This sketch implements a display for the CO2 concentration
 * using an Adafruit Neo Pixel (ring).
 * This aids in deciding whne it is time to letting in fresh air
 * into the room.
 * Inspired by the project from https://co2ampel.org
 * created at November 2020
 *
 */


// the GPIO pin, the pixel ring is connected to.
#define NEO_PIXEL_PIN 0
#define uS_TO_S_FACTOR 1000000
// Altitude above sea level in meter
#define ALTITUDE 60

// #define DEBUG true

// function declarations
void displayCO2Value();
void serialPrintAirMeasurements();
void initSCD30(uint16_t measurementInterval);
void initRTC();
void powerOnStartup();

typedef enum {
  RUN,
  SETUP
} run_modus_t;
run_modus_t runModus = RUN;

//The default I2C address for the SCD30 is 0x61.
SCD30 airSensor;
// (default NeoPixel type: type=NEO_GRB + NEO_KHZ800))
NeoPixel_Gauge gauge = NeoPixel_Gauge(100, CLOCKWISE, 24, NEO_PIXEL_PIN, NEO_GRB + NEO_KHZ800);

RTC_DS3231 rtc;
FileLogger fileLogger = FileLogger();
// Setup a new OneButton on pin ??.
OneButtonSettings settingsButton(3, &fileLogger); //, false, false);

TimeSpan start;
DateTime dateTime;

void setup() {
  WiFi.mode(WIFI_OFF);
  WiFi.forceSleepBegin(); // Wifi off
  delay(1);

  Serial.begin(74880);
  settingsButton.begin();

  pinMode(LED_BUILTIN, OUTPUT); // use LED on board for indicating some events
  digitalWrite(LED_BUILTIN, HIGH); // but switch it off on startup


  Wire.begin();           // Init I2C-Bus
  if (Wire.status() != I2C_OK) {
    Serial.println("Something wrong with the I2C");
    blinkInternal(20, 90, 50);
  }
  gauge.begin();          // Init Neopixel

  settingsButton.tick();
  if (settingsButton.isPressed()) {
    Serial.print("main: Button pressed on start!\n");
    blinkInternal(10, 50, 30);
    runModus = SETUP;
    settingsButton.startSetup();
    // set 80 msec. debouncing time. Default is 50 msec.
    settingsButton.setDebounceTicks(80);
  } else {

    // display time on initial power up only, not when waking up
    if(ESP.getResetInfoPtr()->reason != REASON_DEEP_SLEEP_AWAKE) {
      powerOnStartup();
      ESP.deepSleep(25 * uS_TO_S_FACTOR, WAKE_RF_DISABLED);
    } else {
      dateTime = rtc.now();
      start = TimeSpan(dateTime.secondstime());
      initSCD30(20);
    }
  }
}

/**
 * the main loop
 */
void loop() {
  if (runModus == SETUP) {
    settingsButton.loop();
    if(settingsButton.isSetupFinished()) {
      runModus = RUN;
      settingsButton.clearFinished();
      blinkInternal(3, 80, 280);
      Serial.print("main: Settings menu ended!\n");
      powerOnStartup();
    }
    delay(100);
  } else {

    displayCO2Value();
    yield();

    // log record to storage every second minute
    // using bit mask for modulo 2
    if(!(dateTime.minute() & 1)) {
      if (fileLogger.getLastLoggedMinute() != dateTime.minute()) {
        AirQualityRecord record;
        record.co2 = airSensor.getCO2();
        record.temperature = airSensor.getTemperature();
        record.humidity = airSensor.getHumidity();
        fileLogger.addDataPoint(&dateTime, &record);
      }
    }

    if(dateTime.minute() % 10 == 0) {
      // persist records to SD card every full 10th minute of the hour
      // some records are potentially lost when the device is switched
      // off but writing to SD is supposed to use a lot of milliamps ..
      fileLogger.begin();
      fileLogger.writeValuesToFile(&dateTime);
      // OPTIONAL !!
      // display the time every five minutes only
      // since each single pixel represents 2.5
      // minutes making the display rather coarse
      gauge.displayTime(&dateTime);
    }

    // going to deep sleep for the remainder of the 30 seconds
    // at which a display refresh is being desired
    // deep sleep saves some mA although the display is the
    // most energy hungry module
    TimeSpan diff = TimeSpan(rtc.now().secondstime()) - start;
    uint32_t duration = diff.totalseconds();
  #ifdef DEBUG
    Serial.printf("loop took %i seconds.\n", duration);
  #endif
  if (duration < 30) {
    uint8_t sleepTime = 30 - duration;
  #ifdef DEBUG
    Serial.printf("Going to sleep for %u seconds.\n", sleepTime);
  #endif
      // send the "ampel" to sleep for about half a minute (max)
      // and ensure the WiFi is still powered down after wake up
      ESP.deepSleep(sleepTime * uS_TO_S_FACTOR, WAKE_RF_DISABLED);
      // For the device to be woken up make sure the GPIO16 pin is
      // connected with the RESET pin.

      // every code beyond this sleep won't be reached
      Serial.print("xxxxxxxxxxxxxxxxxxxxThis shoul never been printed   xxxxxxxxxxxxxxxxxxxxxxxxxxxx\n");
    }
    // the init plus loop should never really exceed 30 seconds
    // but if it does, this makes sure the device is not prevented from sleeping
    dateTime = rtc.now();
    start = TimeSpan(dateTime.secondstime());
  }
  delay(100);
}


/**
 * Read CO2 value from sensor and
 * display it on pixel ring if available
 */
void displayCO2Value() {
  if (airSensor.dataAvailable()) {
    uint16_t co2Value = airSensor.getCO2();
    if (co2Value>0)
      gauge.displayValue(co2Value);
#ifdef DEBUG
    Serial.printf("Read co2value: %u\n", co2Value);
    serialPrintAirMeasurements();
  } else {
    Serial.printf("Couldn't read CO2-value from sensor\n");
#endif
  }
}


/**
 * Report read values to serial console
 */
void serialPrintAirMeasurements() {
  Serial.print("co2(ppm): ");
  Serial.print(airSensor.getCO2());

  Serial.print(" | temp(C): ");
  Serial.print(airSensor.getTemperature(), 1);

  Serial.print(" | humidity(%): ");
  Serial.print(airSensor.getHumidity(), 1);

  Serial.print("\n");
}


/**
 * Initialization fo the CO2 sensor
 */
void initSCD30(uint16_t measurementInterval) {
  // setup I2C bus to talk to SCD30 sensor
  Wire.setClock(100000L);            // 100 kHz SCD30
  Wire.setClockStretchLimit(200000L);// CO2-SCD30

  bool airSensorInitialized = false;
  // allow the sensor some time to wake up
  for(uint8_t t = 0; t < 6; t++) {
      if (airSensor.begin()) {
        airSensorInitialized = true;
        break;
      }
#ifdef DEBUG
      blinkInternal(2, 900, 200);
#endif
  }
  if(!airSensorInitialized) {
    Serial.print("Air sensor not detected. Please check wiring\n");
    blinkInternal(10, 200, 100);
  }

  // The minium intervall for the SCD30 are two seconds
  // since the display is refreshed ever half a minute,
  // the measurement intervall needn't be that frequent
  airSensor.setMeasurementInterval(measurementInterval);
  airSensor.setAltitudeCompensation(ALTITUDE);
  airSensor.beginMeasuring();
}


/**
 * Initialization of the real time clock
 */
void initRTC() {
  bool rtcInitialized = false;
  for(uint8_t t = 0; t < 6; t++) {
      if (rtc.begin()) {
        rtcInitialized = true;
#ifdef DEBUG
        blinkInternal(2, 300, 600);
#endif
        break;
      }
#ifdef DEBUG
      blinkInternal(2, 700, 150);
#else
      delay(1000);
      yield();
#endif
  }
  if(!rtcInitialized) {
    Serial.print("Couldn't find RTC. Please check wiring.\n");
    blinkInternal(10, 200, 100);
  }

  if (rtc.lostPower()) {
    delay(2000); // allow some time for the rtc to get going
    Serial.println("RTC is NOT initialized, let's set the time to compile time!");
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    delay(1000);
    yield();
  }

}

void powerOnStartup() {
  gauge.displayGreenCross();
  initRTC();
  dateTime = rtc.now();
  gauge.clear();
  gauge.displayTime(&dateTime);
  yield();
  initSCD30(4);
  delay(6000);
#ifdef DEBUG
  Serial.print("Waiting for CO2 Sensor to come live ");
#endif
  while (true)
  {
    if(airSensor.dataAvailable())
      break;
    yield();
    delay(1000);
#ifdef DEBUG
  Serial.print(".");
#endif
  }
#ifdef DEBUG
  Serial.print("\n");
#endif
  // get measurement more frequent for about
  // the first 15 seconds after power on
  bool cleaned = false;
  for(uint8_t i=0; i<3; i++){
    uint16_t co2Value = airSensor.getCO2();
    if (co2Value>0) {
      if (!cleaned) {
        gauge.swipeClean();
        cleaned = true;
      }
      gauge.displayValue(co2Value);
    }
    delay(2500);
    yield();
    delay(2500);
    yield();
  }
  airSensor.setMeasurementInterval(20);
  dateTime = rtc.now();
  start = TimeSpan(dateTime.secondstime());
}


void printTimeDebug() {
  DateTime dateTime = rtc.now();
  Serial.print(dateTime.timestamp(DateTime::TIMESTAMP_TIME));
  Serial.print("\n");
}
