#ifndef CALIBRATION_PLUGIN_H
#define CALIBRATION_PLUGIN_H

#include "OneButtonSettingsPlugin.h"

class CalibrationPlugin : public OneButtonSettingsPlugin {

public:
  CalibrationPlugin(SettingsDisplay* display);

  void tick() override;

  bool click() override;

  void doubleClick() override;

  void longPressedStop() override;

  bool longPressedStart() override;

  void displayMenuEntry(pluginState_t state) override;
};

#endif // CALIBRATION_PLUGIN_H
