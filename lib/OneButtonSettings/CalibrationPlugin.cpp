#include "CalibrationPlugin.h"

#include <Arduino.h> // for serial

CalibrationPlugin::CalibrationPlugin(SettingsDisplay* display)
  : OneButtonSettingsPlugin(display) {
}

void CalibrationPlugin::tick() {}

bool CalibrationPlugin::click() {
  Serial.print("Start Calibration \n");
  return true; // change to false once the functionality is implemented
}

void CalibrationPlugin::doubleClick() {
  Serial.print("Calibration Plugin doubleClick \n");

}

void CalibrationPlugin::longPressedStop() {

}

bool CalibrationPlugin::longPressedStart() {
  return false;
}

void CalibrationPlugin::displayMenuEntry(pluginState_t state) {
  m_display->display('C', state);
}
