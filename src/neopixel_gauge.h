#ifndef NEO_PIXEL_GAUGE_H
#define NEO_PIXEL_GAUGE_H

/*
 * This implements a gauge display using an adafruit Neo Pixel (ring).
 * inspired by the project from https://co2ampel.org
 * created at November 2020
 *
 */

// Base class
// https://github.com/adafruit/Adafruit_NeoPixel
#include <Adafruit_NeoPixel.h>
// Needed the DateTime definition
// for the method displaying the time
#include "RTClib.h"


// last displayed value will be stored in RTC mem to survive
// deep sleep
// needed for a smoother transition (a flowing effect) between
// displaying two different values.
#define LAST_VALUE_RTC_MEM_POSTION 64 // unused left in for documentation
#define LAST_VALUE_RTC_MEM_OFFSET 0
// number of pixels to be shown as pointer
// or the tail of the pointer
#define MAX_POINTER_WIDTH 2

// #define DEBUG true

// the direction, the pixels are arranged arround the NeoPixel ring.
// or the direction, the gauge shall operate to.
typedef enum Direction
{
    CLOCKWISE = 0, ANTI_CLOCKWISE = 1
} Direction_t;

// colour definition for each value depending on value
static const uint32_t PROGMEM _GaugeScaleColours[24] = {
    0x000200, 0x000200, 0x000200, 0x000200, //  1 -  4
    0x010300, 0x020300, 0x030300, 0x030200, //  5 -  8
    0x040200, 0x030200, 0x030200, 0x030200, //  9 - 12
    0x020100, 0x020100, 0x040100, 0x050100, // 13 - 16
    0x030000, 0x030000, 0x030000, 0x030000, // 17 - 20
    0x030001, 0x030001, 0x030001, 0x030001  // 21 - 24
};
static const uint32_t PROGMEM _GaugeScaleColoursBright[24] = {
    0x001200, 0x001200, 0x001200, 0x001200, //  1 -  4
    0x061000, 0x091000, 0x0D1600, 0x150F00, //  5 -  8
    0x170E00, 0x190E00, 0x1F0F00, 0x220E00, //  9 - 12
    0x260900, 0x280600, 0x290500, 0x290400, // 13 - 16
    0x290200, 0x2C0100, 0x2C0000, 0x340100, // 17 - 20
    0x320102, 0x330102, 0x340102, 0x350102  // 21 - 24
};


class NeoPixel_Gauge : public Adafruit_NeoPixel {

    public:

        /**
         *  Constructor
         *
         *  resolution: value, one single pixel represents
         *  direction:  (default: clockwise)
         *  number of LEDs
         *  pin number: GPIO number the data input is connected to
         *  LED type: See base class
        */
        NeoPixel_Gauge(uint16_t resolution, Direction_t direction,
                uint16_t numPixel, uint16_t pin, neoPixelType type);

        /**
         * Display of a value on the ring
         */
        void displayValue(uint16_t val);

        /**
         * Display the given time
         * currently for 5 seconds and implemented for a 24 pixel ring
         * with the pixel #0 in 'south' direction
         */
        void displayTime(DateTime* dateTime);

        /**
         *      *
         *   *     *
         *      *
         */
        void displayGreenCross();

        /**
         * Simulates swipe cleaning a blackboard.
         */
        void swipeClean();
    
    private:
        void showValue(uint8_t val);

    private:
        uint16_t resolution;
        uint8_t direction;
        uint32_t last_value;

};

#endif //NEO_PIXEL_GAUGE_H
