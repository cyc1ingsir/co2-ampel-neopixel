#ifndef CLOCK_CALIBRATION_PLUGIN_H
#define CLOCK_CALIBRATION_PLUGIN_H

#include "OneButtonSettingsPlugin.h"

class ClockCalibrationPlugin : public OneButtonSettingsPlugin {

public:
  ClockCalibrationPlugin(SettingsDisplay* display);

  void tick() override;

  bool click() override;

  void doubleClick() override;

  void longPressedStop() override;

  bool longPressedStart() override;

  void displayMenuEntry(pluginState_t state) override;

private:

  bool syncOnceWithNTP();

  bool connectToWiFi();

};

#endif // CLOCK_CALIBRATION_PLUGIN_H
