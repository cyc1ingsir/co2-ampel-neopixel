# CO2 Ampel

This is an implementation of a CO2 measurement device inspired by the <http://co2ampel.org> project.  
The main aim of this implementation was to fokus on the possiblity to run the
device independently (off grid so to speak) of mains power and without WiFi.

![CO2 Ampel](doc/ampel-front.jpg)  

## License

Not quite decided, definitely some open source license, preferably GPL v3 or perhaps MIT as this seems to be common at the Arduino world.

## Startup Time

Right after power on, the current time is displayed and the CO2 sensor is initialized in the background.
As soon, data is available from the sensor the display will be cleared and the co2 value is displayed.
For about 15 seconds after the first time, a measurement could be retrieved from the sensor, new date is tried to get more frequently, before the ampel is send to deep sleep for the first time.
It still may take a few seconds before a value is being displayed and the value displayed may need some cycles before it settles.

## Settings Menu

The current implementation includes a settings menu which can be brought to life
when pressing and holding the push botton whilst connecting the power bank.  
Once the green # cross is display on the led ring, the settings are active.  
The menu offers three actions which can be cycled through by a short click on
the button. A long press (roughly 2 seconds) will activate the option and a
further single click will start the action.
When the green # is displayed, the menu can be left by a long press on the
button.
At the moment three options are included.

### Upload (U)

Once, the Ampel is in reach of a known WiFi AP previously connected to via e.g.
the WiFi-Manager sketch, data can be uploaded for analysis to a Influx DB.

#### Precondtions

An Influx DB has been setup on a (local) server.  
Grafana has been setup on a (local) server and a Influx data source has been setup.  
A copy of `lib/OneButtonSettings/InfluxDBConfig_template.h` (without the
`_template` part) with values of your own local setup needs to be provided.  
A sample sketch from e.g. (https://platformio.org/lib/show/6915/ESP_WiFiManager)
has been loaded once to the board and WiFi's credentials have been stored on the
board before flashing the current firmware.

### Calibration (C)

This is for calibrating the sensor but not implemented yet. It will not require
WiFi setup.

### Synchronise the RTC clock via NTP (O)

WiFi credentials need to be stored as described in the Upload's preconditions.
Once this is done, the device connects to a NTP server and adjust the RTC
accordingly. UTC is used internally.
However, the code is setup to talk to 'de.pool.ntp.org' inside
`lib/OneButtonSettings/ClockCalibrationPlugin.cpp`.  
The timezones used to display the clock every 10 minutes is hardcoded to CET
inside `src/neopixel_gauge.cpp` and needed to be changed when used outside this
timezone as well.  

## Parts

It uses an Adafruit NeoPixel ring (with 24 pixels), the famous SCD30 CO2-Sensor from Sensiron.  
The Microcontroller board used is the Wemos D1 mini pro (based on an ESP8266) and an optional DS3231 module is used as RTC. This was mainly added to be able to log the data to a SDCard later including timestamps without the need for any network access. The uSD-card breakout module used is the one from Watterott electronic. The once accepting 3V as VCC.

## IDE

It's based on the arduino framework but <https://platformio.org/> was used for development.  
[CuteCom](https://gitlab.com/cutecom/cutecom) is being used as serial monitor ;-).

## Energy Consumption

The device uses about 20 - 22mA during sleep when the pointer is two pixels wide. During the short time, the time is being displayed, about 38mA is drawn.  
Every time the sensor is performing a new measurement, 77 - 98mA will be drawn but for a very short time.  
With this consumption it should survive hopefully two school days being powered from a 2000mAh USB power bank.

## Wiring

![schematic](doc/schematic.png)
![wiring](doc/Breadboard_wiring.png)
![scale face](doc/scale.svg)
![grafana_dashboard](doc/grafana_dashboard.png)

## Pictures

![Ampel front view](doc/ampel-front.jpg) ![Displaying 800ppm](doc/ampel-800ppm.jpg) ![Ampel from the rear](doc/ampel-rear.jpg) ![Calibration settings menu item](doc/calibration-display.jpg)  

