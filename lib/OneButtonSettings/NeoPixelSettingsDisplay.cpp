#include "NeoPixelSettingsDisplay.h"

#include <Arduino.h> // for Serial!

NeoPixelSettingsDisplay::NeoPixelSettingsDisplay()
    :m_neoPixel(new Adafruit_NeoPixel(24, 0, NEO_GRB + NEO_KHZ800)) {
}

void NeoPixelSettingsDisplay::display(unsigned short number) {
  Serial.printf("#> %u\n", number);
}
void NeoPixelSettingsDisplay::display(const char c, pluginState_t state) {
  m_neoPixel->clear();
  uint32_t colour = 0x000900;

  switch (state)
  {
  case PLUGIN_SELECTED:
  colour = 0x000800;
  break;
  case PLUGIN_ACTIVE:
  colour = 0x040400;
  break;
  case PLUGIN_RUNNING:
  colour = 0x060200;
  break;
  case PLUGIN_ERROR:
  colour = 0x070000;
  break;
  default:
    break;
  }
#ifdef DEBUG
  if (active) {
    Serial.printf("#> |%c|\n", c);
  } else {
    Serial.printf("#> %c\n", c);
  }
#endif
  switch (c)
  {
  case 'U':
    m_neoPixel->fill(colour, 0, 7);
    m_neoPixel->fill(colour, 18, 6);
    break;
  case 'C':
    m_neoPixel->fill(colour, 0, 15);
    m_neoPixel->fill(colour, 22, 2);
    break;
  case 'O':
    for (uint8_t x=1; x<24; x+=3) {
      m_neoPixel->fill(colour, x, 2);
    }
    for (uint8_t x=0; x<22; x+=3) {
      m_neoPixel->setPixelColor(x, 0x010004);
    }
    break;
  default:
    colour = 0x090000;
    m_neoPixel->fill(colour, 5, 3);
    m_neoPixel->fill(colour, 17, 3);
    break;
  }
  m_neoPixel->show();
}
void NeoPixelSettingsDisplay::displayCross() {
#ifdef DEBUG
  Serial.printf("-#-\n");
#endif
  m_neoPixel->clear();
  uint32_t colour = 0x000800;
  m_neoPixel->fill(colour, 0, 2);
  m_neoPixel->fill(colour, 5, 3);
  m_neoPixel->fill(colour, 11, 3);
  m_neoPixel->fill(colour, 17, 3);
  m_neoPixel->setPixelColor(23, colour);
  m_neoPixel->show();
}
void NeoPixelSettingsDisplay::clear() {
  m_neoPixel->clear();
  m_neoPixel->show();
}
