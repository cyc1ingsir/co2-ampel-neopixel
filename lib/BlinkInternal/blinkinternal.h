#ifndef BLINK_INTERNAL_H
#define BLINK_INTERNAL_H

#include <Arduino.h>

void blinkInternal(uint8_t times, unsigned long duration, unsigned long on = 0);

#endif // BLINK_INTERNAL