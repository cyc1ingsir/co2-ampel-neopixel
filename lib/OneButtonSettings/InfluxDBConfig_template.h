#ifndef INFLUXDB_CONFIG_H
#define INFLUXDB_CONFIG_H

// copy this file as InfluxDBConfig.h into the same folder
// and update to your local settings

// InfluxDB  server url. Don't use localhost, always server name or ip address.
// E.g. http://192.168.1.48:8086 (In InfluxDB 2 UI -> Load Data -> Client Libraries),
#define INFLUXDB_URL "http://192.168.1.48:8086"

// InfluxDB v1 database name
#define INFLUXDB_DB_NAME "databasename"

#define NODE_NAME "CO2-Ampel"

#define DATA_POINT_NAME "co2-node"
#endif // INFLUXDB_CONFIG_H

