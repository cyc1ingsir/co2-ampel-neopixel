#include "UploadPlugin.h"
#include "InfluxDBAirDataUploader.h"

#include <Arduino.h> // for serial
#include <blinkinternal.h>

bool recordCallback(TimedAirQualityRecord* record, void* caller) {
  return ((UploadPlugin*) caller)->uploadRecord(record);
}

UploadPlugin::UploadPlugin(SettingsDisplay* display, FileLogger* fileLogger)
  : OneButtonSettingsPlugin(display)
  , m_fileLogger(fileLogger) {
}

void UploadPlugin::tick() {}

bool UploadPlugin::click() {
  displayMenuEntry(PLUGIN_RUNNING);
  Serial.print("UPlugin: Start upload \n");
  if(!m_fileLogger->begin()) {
    Serial.print("UPlugin: ... failed!\n");
    error = true;
    return true; // action completed, see at end of method
  }
  m_airDataUploader = new InfluxDBAirDataUploader();
  if (m_airDataUploader->setup(INFLUXDB_URL, INFLUXDB_DB_NAME)) {
    error = !m_fileLogger->retrieveData(recordCallback, this);
#ifdef DEBUG
    Serial.printf("UPlugin: Upload finished %s error\n", (error)? "with" : "without");
#endif
    blinkInternal(2, 80, 150);
  } else {
#ifdef DEBUG
    Serial.print("UPlugin: Connectioin to InfluxDB failed \n");
    blinkInternal(5, 150, 50);
#endif
    error = true;
  }
  delete(m_airDataUploader);
  // optionally return true only on successful upload
  // making it easier to starting again for another try
  // by just one single click.
  return true; // action completed
}

bool UploadPlugin::uploadRecord(TimedAirQualityRecord* record) {
#ifdef DEBUG_UPLOAD
  Serial.printf("UPlugin: Upload: %d %.1f %.1f %u\n", record->unixtime,
    record->air->temperature, record->air->humidity, record->air->co2);
#endif
  if (!m_airDataUploader->writeAirDataPoint(record)) {
#ifdef DEBUG_UPLOAD
  Serial.print("WARN UPlugin: failed to write data point to DB\n");
#endif
    return false;
  }
  return true;
}

void UploadPlugin::doubleClick() {
#ifdef DEBUG
  Serial.print("UPlugin::doubleClick \n");
#endif
}

void UploadPlugin::longPressedStop() {

}

bool UploadPlugin::longPressedStart() {
  return false;
}

void UploadPlugin::displayMenuEntry(pluginState_t state) {
#ifdef DEBUG_UPLOAD
  Serial.printf("UploadPlugin: setDisplay -%s-\n", (active)? "act" : "-");
#endif
  m_display->display('U', state);
}
