#include "InfluxDBAirDataUploader.h"

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>

#include <TZ.h>

InfluxDBAirDataUploader::InfluxDBAirDataUploader()
  :dataPoint(DATA_POINT_NAME) {
}

bool InfluxDBAirDataUploader::setup(const char* dburl, const char* database) {

  // this will wake up WiFi even if device slept with WAKE_RF_DISABLED
  // tip from https://github.com/esp8266/Arduino/issues/3072 (and bakke.online)
  if (!WiFi.isConnected()) {
    WiFi.persistent(true);
    WiFi.forceSleepBegin();
    delay(10);
    WiFi.persistent(false);
    WiFi.forceSleepWake();
    delay(10);
    WiFi.mode(WIFI_STA);
    WiFi.begin();
    delay(10);

  #ifdef DEBUG_WIFI_RECONNECT
    Serial.printf("IDB-A-DU: > %s < - > %s <\n", WiFi.psk().c_str(), WiFi.SSID().c_str());
    struct station_config conf;
    wifi_station_get_config_default(&conf);
    Serial.printf("IDB-A-DU: > %s < - > %s <\n", conf.password, conf.ssid);
  #endif

    uint16_t counter = 0;
    while (!WiFi.isConnected()) {
      delay(250);
      yield();
      counter++;
  #ifdef DEBUG
      if (counter == 10 || counter == 15 || counter == 30) {
        Serial.print("- + - ");
        // Serial.print("WARN IDB-A-DU: trying to reconnect to WiFi!\n");
        // WiFi.reconnect();
      }
  #endif
      if (counter > 40){
        Serial.print("WARN IDB-A-DU: Connection to WiFi not successful!! \n");
        return false;
      }
    }
  }
  // Add constant tags - only once
  dataPoint.addTag("node", NODE_NAME);

  // Check server connection
  if (airDataInfluxClient == nullptr) {
    airDataInfluxClient = new InfluxDBClient(dburl, database);
    // InfluxDB client instance !! V2 -->
    // #define INFLUXDB_BUCKET "bucket"
    // airDataInfluxClient = new InfluxDBClient(dburl, INFLUXDB_ORG, INFLUXDB_BUCKET, INFLUXDB_TOKEN);
  }
  if (airDataInfluxClient->validateConnection()) {
#ifdef DEBUG
    Serial.print("IDB-A-DU: Connected to InfluxDB: ");
    Serial.print(airDataInfluxClient->getServerUrl());
    Serial.print("\n");
#endif
  } else {
#ifdef DEBUG
    Serial.print("IDB-A-DU: InfluxDB connection failed: ");
    Serial.println(airDataInfluxClient->getLastErrorMessage());
#endif
    return false;
  }

  WriteOptions writeOptions = WriteOptions()
    .writePrecision(WritePrecision::S)
    .batchSize(5)
    .bufferSize(5)
    .flushInterval(2)
    .maxRetryInterval(10);

  airDataInfluxClient->setWriteOptions(writeOptions);

  airDataInfluxClient->setHTTPOptions(HTTPOptions().connectionReuse(true));

  return true;
}

bool InfluxDBAirDataUploader::writeAirDataPoint(const TimedAirQualityRecord* record) {

  // Store measured value into point
  dataPoint.clearFields();
  // Report RSSI of currently connected network
  dataPoint.addField("temperature", record->air->temperature);
  dataPoint.addField("humidity", record->air->humidity);
  dataPoint.addField("co2", record->air->co2);
  dataPoint.setTime(record->unixtime);
  if (airDataInfluxClient != nullptr) {
#ifdef DEBUG
    // Print exact content, we will be writing
    Serial.print("IDB-A-DU: Writing: ");
    Serial.println(airDataInfluxClient->pointToLineProtocol(dataPoint));
#endif
    // If no Wifi signal, try to reconnect to it
    if ((WiFi.RSSI() == 0) && (!WiFi.isConnected())) {
      if (!WiFi.reconnect()) {
        Serial.println("IDB-A-DU: Wifi connection lost");
        return false;
      }
    }
    // Write point
    if (!airDataInfluxClient->writePoint(dataPoint)) {
      Serial.print("IDB-A-DU: InfluxDB write failed: ");
      Serial.println(airDataInfluxClient->getLastErrorMessage());
      return false;
    }
  } else {
#ifdef DEBUG
     Serial.print("ERROR IDB-A-DU: airDateInfluxClient is nullptr\n");
#endif
    return false;
  }
  return true;
}

InfluxDBAirDataUploader::~InfluxDBAirDataUploader() {
  if(airDataInfluxClient == nullptr) {
    return;
  }
  if (!airDataInfluxClient->isBufferEmpty()) {
      // Write all remaining points to db
      airDataInfluxClient->flushBuffer();
  }
  delete airDataInfluxClient;
  airDataInfluxClient = nullptr;
  // prevent same information to be written to the EEPROM over and over
  WiFi.persistent(false);
  delay(1);
  WiFi.disconnect(true);
  delay(10);
  WiFi.forceSleepBegin();
  delay(10);
}
