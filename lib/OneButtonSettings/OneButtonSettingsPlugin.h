#ifndef ONE_BUTTON_SETTINGS_PLUGIN_H
#define ONE_BUTTON_SETTINGS_PLUGIN_H

#include "SettingsDisplay.h"

class OneButtonSettingsPlugin {

public:
  OneButtonSettingsPlugin(SettingsDisplay* display) { m_display = display; }

  virtual void tick();

  virtual bool click();

  virtual void doubleClick();

  virtual void longPressedStop();

  virtual bool longPressedStart();

  virtual void displayMenuEntry(pluginState_t state = PLUGIN_SELECTED);

  bool hasHadError() {return error;}

protected:
  SettingsDisplay* m_display;

  bool error = false;

};

#endif // ONE_BUTTON_SETTINGS_PLUGIN_H
