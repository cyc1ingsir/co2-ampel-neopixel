#ifndef INFLUXDB_AIR_DATA_UPLOADER_H
#define INFLUXDB_AIR_DATA_UPLOADER_H

#include <InfluxDbClient.h>
#include <airDataTypes.h>
#include "InfluxDBConfig.h"

class InfluxDBAirDataUploader {
  public:
    InfluxDBAirDataUploader();
    ~InfluxDBAirDataUploader();

    bool setup(const char* dburl, const char* database);

    bool writeAirDataPoint(const TimedAirQualityRecord* record);

  private:
    // InfluxDB client instance for InfluxDB 1
    InfluxDBClient* airDataInfluxClient = nullptr;

    // Data point
    Point dataPoint;

};

#endif // INFLUXDB_AIR_DATA_UPLOADER_H
