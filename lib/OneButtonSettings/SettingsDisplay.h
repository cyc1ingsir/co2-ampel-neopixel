#ifndef SETTINGS_DISPLAY_H
#define SETTINGS_DISPLAY_H

typedef enum {
  PLUGIN_SELECTED,
  PLUGIN_ACTIVE,
  PLUGIN_RUNNING,
  PLUGIN_ERROR
} pluginState_t;

/**
 * Pure virutal interface class
 * Defining all methods a display needs to implement
 * for visualizing settings
 */
class SettingsDisplay {

public:
  virtual void display(unsigned short number);

  virtual void display(const char c, pluginState_t state);

  virtual void displayCross();

  virtual void clear();

};

#endif // SETTINGS_DISPLAY_H
